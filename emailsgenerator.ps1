﻿$Emails = Get-Content -Path "~\Desktop\corpstoreemails.txt"
$BodyTemplate = Get-Content C:\Users\cooper.worobetz\Desktop\Template.htm

Foreach ($Email in $Emails){
    $Username = $Email.Split('@')[0]
    $Password = $Email.Split('@')[0] + 'TG!'

    $Body = $Email | ForEach-Object { $BodyTemplate -replace '@@@USERNAME@@@', $Username }
    $Body = $Email | ForEach-Object { $Body -replace '@@@PASSWORD@@@', $Password }

    $Outlook = New-Object -comObject Outlook.Application
    $Mail = $Outlook.CreateItem(0)
    $Mail.Subject = "Tommy Gun's Service Desk Introduction"
    $Mail.HTMLBody = [string]$Body
    $Mail.Attachments.Add("C:\Users\cooper.worobetz\Desktop\Tommy Gun’s Service Desk.pdf")
    $Mail.Recipients.Add($Email)
    $Mail.save()

    $Inspector = $Mail.GetInspector
    $Inspector.Display()
}