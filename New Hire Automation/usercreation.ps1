# This script is currently geared towards new, hourly, trim employees. As such, 
# it formats email addresses as flastname@flowr.ca and sets their City, Country, Title and LicenseAssignment as such.
# This script requires a newemployees.txt file containing the new trim employee's names, line by line.
# The script will output to a CSV file output.csv. This output includes email addresses, passwords, etc.
# Make sure you have enough licenses purchased before starting.

# Variables
$UsageLocation = "CA"
$Country = "Canada"
$City = "Kelowna"
$Title = "Trim Labourer"
$License = "flowr:DESKLESSPACK"

$Credential = Get-Credential
Connect-MsolService -Credential $UserCredential

$UserList = Get-Content -Path employees.txt

ForEach ($User in $UserList){
    $FirstInitial = $User[0]
    $FirstName = $User.Split()[0]
    $LastName = $User.Split()[1]
    $Email = (($FirstInitial + $LastName).ToLower()) + "@flowr.ca"
    
    New-MsolUser -DisplayName $User -FirstName $FirstName -LastName $LastName -UserPrincipalName $Email -UsageLocation $UsageLocation -City $City -Country $Country -Title $Title -LicenseAssignment $LicenseAssignment | Export-Csv -Append -Path .\output.csv -NoClobber
}