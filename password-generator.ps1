﻿$Words = Get-Content ~\Desktop\words.txt
$SpecialChars = "!","@","#"

ForEach ($Word in $Words) {
    if ($Word.Contains('-') -or $Word.Length -gt 6 -or $Word.Length -lt 4){
        continue
    }
    $Word = $Word[0].toString().toUpper() + $Word.Substring(1,$Word.Length - 1)
    $RandomNum = Get-Random -Minimum 0000 -Maximum 9999
    while ($RandomNum.ToString().Length -lt 4) {
        $RandomNum = '0' + $RandomNum.ToString()
    }
    $Password = $Word + $RandomNum + $SpecialChars[(Get-Random -Minimum 0 -Maximum ($SpecialChars.Length))]
    $Password
}